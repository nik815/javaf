#!/bin/bash


echo "Thank You for Using this Program!"


if [ ! command -v java &> /dev/null ]
then
    echo "java is not installed"
    echo "Please install Java(jdk) before using this"
    exit
else
    echo "Java is found!"
fi

echo "Do you want to create the folder the current Directory? (y/n)?"

read option



package_create(){

        mkdir src
        cd src
        echo " Enter the package name"
        read package
        mkdir $package
        cd $package
        echo " Created main file"
        touch main.java
        echo " Do you want to open the file?(y/n)"
}



if [ $option == y ]
    then
        echo "Please enter the project name.."
        read project_name
        mkdir $project_name
        cd $project_name
        package_create
        read choice
        if [ $choice == y ]
        then
            echo "Please enter the program name"
            read program
            $program main.java
        fi

elif [ $option == n ]
    then
        echo "Please write the full path(including the project name) you wish to create"
        read path_project_name
        mkdir $path_project_name 
        cd $path_project_name
        package_create
        read choice
        if [ $choice == y ]
        then
            echo "Please enter the program name"
            read program
            $program main.java
        fi

fi
